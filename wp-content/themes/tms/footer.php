<?php if ( is_active_sidebar( 'giga-store-footer-area' ) ) { ?>
	<div class="footer-widgets"> 
		<div class="container">		
			<div id="content-footer-section" class="row clearfix">
				<?php dynamic_sidebar( 'giga-store-footer-area' ) ?>
			</div>
		</div>
	</div>	
<?php } ?>
<footer id="colophon" class="rsrc-footer" role="contentinfo">
	<div class="container">  
		<div class="row rsrc-author-credits">
             
				</div>
			<p class="text-center">
				<center>
				<div><a href="/terms-and-conditions/">Social Media</a></div><br><br>
				</center>
				</p> 
		</div>
	</div>       

		<div class="row rsrc-footer-before">
			<p class="text-center">
				<center>
				<div>Copyright 2018 @ <a href="http://trackmyselfie.com" target="_blank">Track My Selfie</a></div>
				</center>
				</p> 
             
		</div>
	</div>       


</footer> 


<p id="back-top">
	<a href="#top"><span></span></a>
</p>
<!-- end main container -->
</div>
<nav id="menu" class="off-canvas-menu">
	<?php
	wp_nav_menu( array(
		'theme_location' => 'main_menu',
		'container'		 => false,
	) );
	?>
</nav>
<?php wp_footer(); ?>
</body>
</html>
